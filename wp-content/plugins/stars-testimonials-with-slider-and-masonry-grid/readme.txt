=== Stars Testimonials with Slider and Masonry Grid ===
Contributors: Rameez_Iqbal
Donate link: https://www.paypal.me/webcodingplace
Tags: feedback, testimonials, ratings, stars, comments, visual composer, theme, thumbnail
Requires at least: 3.5
Tested up to: 4.9
Stable tag: 2.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

15+ Ready to use Testimonial Layouts with Stars, Sliders and Masonry Grid

== Description ==

This plugin will let you to display feedbacks and ratings of your clients on your site with some beautiful themes. A collection of 15+ styles with customize options to suit your needs. This plugin is also compatible with Visual Composer to easily embed shortcodes.

<h2>Quick Links</h2>
<ul>
	<li><a href="https://demos.webcodingplace.com/stars-testimonials-wordpress-plugin-all-styles/">Demo with all Styles</a></li>
	<li><a href="https://demos.webcodingplace.com/stars-testimonials-wordpress-plugin-masonry-grid/">Masonry Grid</a></li>
	<li><a href="https://demos.webcodingplace.com/stars-testimonials-wordpress-plugin-slider/">Slider Carousel</a></li>
	<li><a href="https://demos.webcodingplace.com/how-to-use-stars-testimonial-wordpress-plugin/">How to Use</a></li>
</ul>

== Installation ==

1. Go to plugins in your dashboard and select 'add new'
2. Search for 'Stars Testimonials' and install it
3. Go to Dashboard > Stars Testimonials Menu, and add testimonials here
4. You can also arrage testimonials in categories
5. Use this shortcode to display testomonial [stars_testimonials]



== Screenshots ==

1. Admin Settings
2. Frontend Display
3. Without Thumbnails
4. Visual Composer Compatible

== Changelog ==

= 2.0 - 21 October 2017 =
* Bug Foxed: CSS conflict with slider is resolved

= 1.0 - 30th August 2017 =
* Initial Release