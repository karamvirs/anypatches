<?php
// Uploaded files preview rendering
$non_images_uploads = array();
$title_html = "<h4>".__('Uploaded images:', 'woocommerce-files-upload')."</h4>";
$title_html_rendered = false;
foreach($current_product_uploads as $upload)
{
	//Image preview
	foreach($upload['url'] as $index => $upload_url)
		if($wcuf_media_model->is_image($upload_url))
		{
			if(!$title_html_rendered)
			{
				$title_html_rendered = true;
				echo $title_html;
			}
			echo "<div class='wcuf_image_preview_container'>
						<img class='wcuf_image_preview' src='{$upload_url}'  width='120' />
						<span class='wcuf_file_name_text'>".$upload['original_filename'][$index]."</span>
						<span class='wcuf_file_quantity_text'>".__('Quantity: ','woocommerce-files-upload').$upload['quantity'][$index]."</span>
				  </div>";
		}
		else 
			$non_images_uploads[$index] =  $upload_url;
}

//Non images preview
if(!empty($non_images_uploads))
{
	echo "<h4>".__('Uploaded files list:', 'woocommerce-files-upload')."</h4>";
	echo "<ol class='wcuf_non_image_list'>";
	foreach($non_images_uploads as $upload_field_id => $upload_url)
	{
		echo "<li class='wcuf_non_image_element'>
					<span class='wcuf_file_name_text'>".$upload['original_filename'][$upload_field_id]."</span>
					<span class='wcuf_file_quantity_text'>".__('Quantity: ', 'woocommerce-files-upload').$upload['quantity'][$index]."</span>					
			  </li>";
	}
	echo "</ol>";
}
	
?>