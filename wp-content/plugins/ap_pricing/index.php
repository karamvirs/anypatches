<?php
/*
  Plugin Name: Any Patches Pricing
  Description: Any Patches Plugin. Problem related to plugin contact us.
  Version: 1.0
  Author: Eshan Joshi
 */

  
// function to create the DB / Options / Defaults					
function ap_pricing_install() {
   	global $wpdb;
  	$ap_pricing_table = $wpdb->prefix . 'pricing';
 
	// create the ECPT metabox database table
	if($wpdb->get_var("show tables like '$ap_pricing_table'") != $ap_pricing_table) 
	{
		$sql = "CREATE TABLE " . $ap_pricing_table . " (
		`prc_id` int(11) NOT NULL AUTO_INCREMENT,
		`product_name` varchar(1000) NOT NULL,
		`product_type` varchar(1000) NOT NULL,
		`size` float NOT NULL,
		`quantity` int(11) NOT NULL,
		`price` float(23,2) NOT NULL,
		`prc_date_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		UNIQUE KEY prc_id (prc_id)
		);";
 
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
	}
 
}
// run the install scripts upon plugin activation
register_activation_hook(__FILE__,'ap_pricing_install');

 
if ($_GET['page'] == 'product-pricing') {
    wp_register_script('bootstrap_js', WP_PLUGIN_URL . "/ap_pricing/js/bootstrap.min.js");
    wp_enqueue_script('bootstrap_js', false);

    wp_register_script('change_script', WP_PLUGIN_URL . '/ap_pricing/js/change_script.js');
    wp_enqueue_script('change_script', false);

    wp_register_script('custom_js', WP_PLUGIN_URL . '/ap_pricing/js/custom.js');
    wp_enqueue_script('custom_js', false);
    wp_register_script('data_table_js', WP_PLUGIN_URL . '/ap_pricing/js/jquery.dataTables.min.js');
    wp_enqueue_script('data_table_js', false);

    wp_enqueue_style('custom_bootstraps', WP_PLUGIN_URL . "/ap_pricing/css/bootstrap.min.css");
    wp_enqueue_style('custom_bootstraps', false);
    wp_enqueue_style('style', WP_PLUGIN_URL . '/ap_pricing/css/style.css');
    wp_enqueue_style('style', false);
    wp_enqueue_style('font-awesome', WP_PLUGIN_URL . '/ap_pricing/css/font-awesome.css');
    wp_enqueue_style('font-awesome', false);
    wp_enqueue_style('data_table', WP_PLUGIN_URL . '/ap_pricing/css/jquery.dataTables.min.css');
    wp_enqueue_style('data_table', false);
}

function show_html() {
    global $wpdb;
    $main_table1 = $wpdb->prefix . 'pricing';
    ?>
    <section class="admin_outer">
        <div class="container">
            <div class="row">
                <div class="admin_order">
                    <h2>Add Price For Product</h2>
                    <div class="row admin-from_box">
                        <div class=" col-md-3 col-sm-3 col-xs-12 pd">
                            <div class="form-group">
                                <span class="url" id="<?php echo plugins_url('ap_pricing/get_ajax.php'); ?>"></span>
                                <span class="get_wp_prifix" id="<?php echo $main_table1; ?>"></span>
                                <label class="control-label">Select Product<span class="required"> * </span></label>
                                <select class="form-control text-input" data-required="1" name="prd_name" id="prd_name"  onchange="productTypeChange(this);">
                                    <option value="-1">Select Product</option>
                                    <option value="Embroidered_Patches">Embroidered Patches</option>
									<option value="Woven_Patches">Woven Patches</option>
									<option value="PVC_Patches">PVC Patches</option>
									<option value="Chenille_Patches">Chenille Patches</option>
                                </select>
                            </div>
                        </div>
                        <div class=" col-md-2 col-sm-2 col-xs-12 pd">
                            <div class="form-group">
                                <label class="control-label">Select Type<span class="required"> * </span></label>
                                <select class="form-control text-input" data-required="1" name="prd_type" id="prd_type">
                                    <option value="-1">Select Type</option>
                                </select>
                            </div>
                        </div>
                        <div class=" col-md-2 col-sm-2 col-xs-12 pd">
                            <div class="form-group">
                                <label class="control-label">Select Size (inch)<span class="required"> * </span></label>
                                <select class="form-control text-input" data-required="1" name="prd_size" id="prd_size">
                                    <option value="-1">Select Size</option>
                                    <option value="1">1</option>
                                    <option value="1.5">1.5</option>
                                    <option value="2">2</option>
                                    <option value="2.5">2.5</option>
                                    <option value="3">3</option>
                                    <option value="3.5">3.5</option>
                                    <option value="4">4</option>
                                    <option value="4.5">4.5</option>
                                    <option value="5">5</option>
                                    <option value="5.5">5.5</option>
                                    <option value="6">6</option>
                                    <option value="6.5">6.5</option>
                                    <option value="7">7</option>
                                    <option value="7.5">7.5</option>
                                    <option value="8">8</option>
                                    <option value="8.5">8.5</option>
                                    <option value="9">9</option>
                                    <option value="9.5">9.5</option>
                                    <option value="10">10</option>
                                    <option value="10.5">10.5</option>
                                    <option value="11">11</option>
                                    <option value="11.5">11.5</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                        </div>
                        <div class=" col-md-2 col-sm-2 col-xs-12 pd">
                            <div class="form-group">
                                <label class="control-label">Select Qty. (pcs) <span class="required"> * </span></label>
                                <select class="form-control text-input" data-required="1" name="prd_qty" id="prd_qty">
                                    <option value="-1">Select Qty</option>
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="200">200</option>
                                    <option value="300">300</option>
                                    <option value="500">500</option>
                                    <option value="1000">1000</option>
                                    <option value="2000">2000</option>
                                    <option value="5000">5000</option>
                                    <option value="10000">10000</option>
                                </select>
                            </div>
                        </div>
                        <div class=" col-md-3 col-sm-3 col-xs-12 pd">
                            <div class="form-group">
                                <label class="control-label">Price <span class="required"> * </span></label>
                                <input name="price"  placeholder="Price enter here" id="prd_price" class="form-control" type="text">
                            </div>
                            <div class="form-group admim_btn">
                                <button type="submit" class="btn green" id="save_prc" value="Submit">Submit</button>
                                <button type="button" id="reset" class="btn default">Reset</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="admin_type_box">
                    <div class="admin_tabs">
                        <label>Product Name : &nbsp;</label>
                        <ul class="custom_tabs">
                            <li><a id="-1" class="change_prd_name admin_active" href="javascript:void(0)">All</a></li>
                            <li><a id="Embroidered_Patches" class="change_prd_name" href="javascript:void(0)">Embroidered Patches</a></li>
                            <li><a id="Woven_Patches" class="change_prd_name" href="javascript:void(0)">Woven Patches</a></li>
                            <li><a id="PVC_Patches" class="change_prd_name" href="javascript:void(0)">PVC Patches</a></li>
                            <li><a id="Chenille_Patches" class="change_prd_name" href="javascript:void(0)">Chenille Patches</a></li>
                        </ul>
						<div class="admin_tabs_prd_type" style="display: none; width: 100%; float: left; margin-top: 10px;" >
						<label>Product Type : &nbsp;</label>
						<ul class="custom_tabs custom_tabs_Embroidered_Patches" style="display:none">
                            <li><a id="-1" class="change_prd_type admin_active" href="javascript:void(0)">All</a></li>
                            <li><a id="50% Embroidery" class="change_prd_type" href="javascript:void(0)">50% Embroidery</a></li>
                            <li><a id="75% Embroidery" class="change_prd_type" href="javascript:void(0)">70% Embroidery</a></li>
                            <li><a id="100% Embroidery" class="change_prd_type" href="javascript:void(0)">100% Embroidery</a></li>
                        </ul>
						<ul class="custom_tabs custom_tabs_PVC_Patches"  style="display:none">
                            <li><a id="-1" class="change_prd_type admin_active" href="javascript:void(0)">All</a></li>
                            <li><a id="2D" class="change_prd_type" href="javascript:void(0)">2D</a></li>
                            <li><a id="3D" class="change_prd_type" href="javascript:void(0)">3D</a></li>
                        </ul>
						</div>	
                    </div>
                    <div class="admin_box_list">

                        <div class="portlet-title">
                            <div class="caption">Manage Product Variations </div>
                        </div>

                        <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                            <thead>
                                <tr>
                                    <th> Sr No. </th>
                                    <th>Name</th>
                                    <th>Size</th>
                                    <th>Qty.</th>
                                    <th>Price</th>
                                    <th align="center"> Action </th>
                                </tr>
                            </thead>
                            <tbody id="itemContainer">
                                <!-- Get AJAX from server -->
                            <div class="verify_data" style="display: none;">
                                <img src="<?php echo plugins_url('ap_pricing/icons/giphy.gif'); ?>"/>
                            </div>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal modal02 fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title">Edit Price For Product</h4>
                    </div>
                    <div class="modal_form">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Select Product<span class="required"> * </span></label>
                                <select class="form-control text-input" data-required="1" name="prd_name" id="edit_prd_name"  onchange="editProductTypeChange(this);">
                                    <option value="-1">Select Product</option>
                                    <option value="Embroidered_Patches">Embroidered Patches</option>
									<option value="Woven_Patches">Woven Patches</option>
									<option value="PVC_Patches">PVC Patches</option>
									<option value="Chenille_Patches">Chenille Patches</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="sel1">Select Type <span class="required"> * </span> </label>
                                <select class="form-control text-input" data-required="1" name="prd_type" id="edit_prd_type">
                                    <option value="-1">Select Type</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="sel1">Select Size (inch)<span class="required"> * </span></label>
                                <select class="form-control text-input" data-required="1" name="prd_size" id="edit_prd_size">
                                    <option value="-1">Select Size</option>
                                    <option value="1">1</option>
                                    <option value="1.5">1.5</option>
                                    <option value="2">2</option>
                                    <option value="2.5">2.5</option>
                                    <option value="3">3</option>
                                    <option value="3.5">3.5</option>
                                    <option value="4">4</option>
                                    <option value="4.5">4.5</option>
                                    <option value="5">5</option>
                                    <option value="5.5">5.5</option>
                                    <option value="6">6</option>
                                    <option value="6.5">6.5</option>
                                    <option value="7">7</option>
                                    <option value="7.5">7.5</option>
                                    <option value="8">8</option>
                                    <option value="8.5">8.5</option>
                                    <option value="9">9</option>
                                    <option value="9.5">9.5</option>
                                    <option value="10">10</option>
                                    <option value="10.5">10.5</option>
                                    <option value="11">11</option>
                                    <option value="11.5">11.5</option>
                                    <option value="12">12</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="sel1">Select Qty. (pcs) <span class="required"> * </span></label>
                                <select class="form-control text-input" data-required="1" name="prd_qty" id="edit_prd_qty">
                                    <option value="-1">Select Qty</option>
                                    <option value="5">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="200">200</option>
                                    <option value="300">300</option>
                                    <option value="500">500</option>
                                    <option value="1000">1000</option>
                                    <option value="2000">2000</option>
                                    <option value="5000">5000</option>
                                    <option value="10000">10000</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="usr">Price <span class="required"> * </span></label>
                                <input type="text" class="form-control" id="edit_prd_prc" name="price">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group form-group02">
                                <button id="update_pricing" id="" class="btn green" type="button">Update</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php
}

//admin Menu code
function my_menu() {
    add_menu_page('Product Pricing', 'Pricing', 'manage_options', 'product-pricing', 'show_html', plugins_url('ap_pricing/icons/cart.png'));
}

add_action('admin_menu', 'my_menu');

//End admin menu code

?>