/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {
    $(".error").hide();
    $(".icon").hide();
    $(".errormsg").hide();
    $(".successmsg").hide();
    $(".save_data").click(function () {
        var url = $(this).attr('id');
        var name = $(".user_name").val();
        var user_email = $(".user_email").val();
        var mobile = $(".user_mob").val();
        var subject = $(".user_sub").val();
        var message = $(".user_msg").val();
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        var total = 0;
        if (name == "") {
            $("#name").show();
            total++;
        }
        if (user_email == "") {
            $("#email_test").show();
            total++;
        }
        if (mobile == "") {
            $("#mobile").show();
            total++;
        }
        if (isNaN(mobile)) {
            total++;
        }
        if (message == "") {
            $("#msg").show();
            total++;
        }
        if (subject == "") {
            $("#subject").show();
            total++;
        }
        if (total != 0) {
            return false;
        } else {
            if (!emailReg.test(user_email)) {
                $("#email_test").show();
                $("#email_test").html("Please enter a valid email address");
                return false;
            }
            $(".icon").show();
            var send_data = "user_name=" + name + "&user_email=" + user_email + "&mobile=" + mobile + "&subject=" + subject + "&messsage=" + message;
            $.ajax({
                url: url,
                type: 'POST',
                data: send_data,
                success: function (resp) {
                    if (resp == 2) {
                        $(".icon").hide();
                        $(".errormsg").show();
                        $(".errormsg").addClass('msg_err');
                        $(".errormsg").html("Failed to send your message. Please try later or contact the administrator by another method.");

                    } else {
                        $(".icon").hide();
                        $(".successmsg").show();
                        $(".successmsg").addClass('msg_scs');
                        $(".successmsg").html("Message has been sent successfully.");
                        $(".user_name").val('');
                        $(".user_email").val('');
                        $(".user_mob").val('');
                        $(".user_msg").val('');
                    }

                },
                error: function () {
                    alert("There is some technical problem");
                }
            });
        }

    });

    $(".user_name").keyup(function () {
        $("#name").hide();
        $(".errormsg").hide();
        $(".successmsg").hide();
    });

    $(".user_email").keyup(function () {
        var email = $(this).val();
        if (email == 0) {
            $("#email_test").show();
            $("#email_test").html("Please enter email id");
            $(".errormsg").hide();
            $(".successmsg").hide();
        }
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        if (!emailReg.test(email)) {
            $("#email_test").show();
            $("#email_test").html("Please enter a valid email address");
            $(".errormsg").hide();
            $(".successmsg").hide();
        } else {
            $("#email_test").hide();
            $(".errormsg").hide();
            $(".successmsg").hide();
        }
    });

    $(".user_mob").keyup(function () {
        var number = $(this).val();
        if (isNaN(number)) {
            $("#mobile").show();
            $("#mobile").html("Please enter a valid mobile number");
            $(".errormsg").hide();
            $(".successmsg").hide();
        } else {
            $("#mobile").hide();
            $(".errormsg").hide();
            $(".successmsg").hide();
        }
    });

    $(".user_sub").keyup(function () {
        $("#subject").hide();
        $(".errormsg").hide();
        $(".successmsg").hide();
    });

    $(".user_msg").keyup(function () {
        $("#msg").hide();
        $(".errormsg").hide();
        $(".successmsg").hide();
    });

});


//Setting page

$(function () {
    $(".change_error").show();
    $(".admin_email").hide();
    $(".error_msg").click(function () {
        $(".change_error").show(400);
        $(".admin_email").hide(400);
    });
    $(".email_admin").click(function () {
        $(".change_error").hide(400);
        $(".admin_email").show(400);
    });
});

$(function () {
    $(".loading").hide();
    $(".save_changes").click(function () {
        var url = $(this).attr('name');
        var changes_name = $(".change_name").val();
        var changes_email = $(".change_email").val();
        var changes_mobile = $(".change_mobile").val();
        var changes_subject = $(".change_subject").val();
        var changes_message = $(".change_msg").val();
        var changes_send_success = $(".change_success").val();
        var changes_send_error = $(".change_values_error").val();
        if (changes_name == "") {
            $(".show_image").show();
            $(".show_message").html("Please type error message for name");
        } else if (changes_email == "") {
            $(".show_image").show();
            $(".show_message").html("Please type error message for email");
        }
        else if (changes_mobile == "") {
            $(".show_image").show();
            $(".show_message").html("Please type error message for mobile");
        }
        else if (changes_subject == "") {
            $(".show_image").show();
            $(".show_message").html("Please type error message for subject");
        }
        else if (changes_message == "") {
            $(".show_image").show();
            $(".show_message").html("Please type error message for message");
        }
        else if (changes_send_success == "") {
            $(".show_image").show();
            $(".show_message").html("Please type error message when email sent");
        }
        else if (changes_send_error == "") {
            $(".show_image").show();
            $(".show_message").html("Please type error message when email not sent");
        } else {
            $(".loading").show();
            send_data = "name=" + changes_name + "&email=" + changes_email + "&mobile=" + changes_mobile + "&subject=" + changes_subject + "&success=" + changes_send_success + "&error=" + changes_send_error + "&message=" + changes_message;
            $.ajax({
                url: url,
                type: 'POST',
                data: send_data,
                success: function (resp) {
                    if (resp) {
                        $(".loading").hide();
                        $(".show_image").show();
                        $(".show_message").html("Message has been save successfully");
                        location.reload();
                    }
                }, error: function () {
                    $(".loading").hide();
                    $(".show_image").show();
                    $(".show_message").html("There is some technical problem");
                }
            });
        }
    });
});

$(function () {
    $(".reset_changes").click(function () {
        $(".loading").show();
        var url = $(this).attr('name');
        var reset = "rst";
        var sending_item = "reset=" + reset;
        $.ajax({
            url: url,
            type: 'POST',
            data: sending_item,
            success: function (resp) {
                if (resp) {
                    site_url12=$(".get_site").attr('id');
                    $(".loading").hide();
                    $(".show_image").show();
                    $(".show_message").html("Reset Setting Successfully");
                    window.location.href=site_url12;
                }
            }, error: function () {                
                $(".loading").hide();
                $(".show_image").show();
                $(".show_message").html("There is some technical problem");
            }
        });
    });
});



