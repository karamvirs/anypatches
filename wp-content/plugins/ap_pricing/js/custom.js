/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(document).ready(function ($) {
    show_data();
    function show_data() {
        var get_ajax_url = $(".url").attr('id');
        var get_wp_prifix = $(".get_wp_prifix").attr('id');
        var get_data = "get_wp_prifix=" + get_wp_prifix + "&ret_data=1";
        $(".verify_data").show();
        $("#itemContainer").html('');
        $.ajax({
            url: get_ajax_url,
            type: 'POST',
            data: get_data,
            success: function (resp) {
                $(".verify_data").hide();
                $("#itemContainer").html(resp);
                $('#sample_editable_1').DataTable();
            }, error: function () {
                //alert("There is some problem to retrieve data");
            }
        });
    }

    function show_data_by_name(change_prd_name) {
        var get_ajax_url = $(".url").attr('id');
        var get_wp_prifix = $(".get_wp_prifix").attr('id');
        var get_data = "get_wp_prifix=" + get_wp_prifix + "&ret_data_by_name=1&prd_changed_name=" + change_prd_name;
        $(".verify_data").show();
        $("#itemContainer").html('');
        $.ajax({
            url: get_ajax_url,
            type: 'POST',
            data: get_data,
            success: function (resp) {
                $(".verify_data").hide();
                $("#itemContainer").html(resp);
            }, error: function () {
                //alert("There is some problem to retrieve data");
            }
        });
    }

    //change product by product name
    $(".custom_tabs .change_prd_name").click(function () {
        var change_prd_name = $(this).attr('id');
        $(".custom_tabs .change_prd_name").removeClass("admin_active");
        $(this).addClass("admin_active");
		if(change_prd_name == "Embroidered_Patches"){
			$("ul.custom_tabs.custom_tabs_Embroidered_Patches").show();
			$(".admin_tabs_prd_type").show();
			$("ul.custom_tabs.custom_tabs_PVC_Patches").hide();
		}
		if(change_prd_name == "PVC_Patches"){
			$("ul.custom_tabs.custom_tabs_Embroidered_Patches").hide();
			$("ul.custom_tabs.custom_tabs_PVC_Patches").show();
			$(".admin_tabs_prd_type").show();
		}
		if(change_prd_name == "Woven_Patches"){
			$("ul.custom_tabs.custom_tabs_Embroidered_Patches").hide();
			$("ul.custom_tabs.custom_tabs_PVC_Patches").hide();
			$(".admin_tabs_prd_type").hide();
		}
		if(change_prd_name == "Chenille_Patches"){
			$("ul.custom_tabs.custom_tabs_Embroidered_Patches").hide();
			$("ul.custom_tabs.custom_tabs_PVC_Patches").hide();
			$(".admin_tabs_prd_type").hide();
		}
        show_data_by_name(change_prd_name);
    });
    function show_data_by_type(change_prd_type) {
        var get_ajax_url = $(".url").attr('id');
        var get_wp_prifix = $(".get_wp_prifix").attr('id');
        var get_data = "get_wp_prifix=" + get_wp_prifix + "&ret_data_by_type=1&prd_changed_type=" + change_prd_type;
        $(".verify_data").show();
        $("#itemContainer").html('');
        $.ajax({
            url: get_ajax_url,
            type: 'POST',
            data: get_data,
            success: function (resp) {
                $(".verify_data").hide();
                $("#itemContainer").html(resp);
            }, error: function () {
                //alert("There is some problem to retrieve data");
            }
        });
    }

    //change product by product type
    $(".custom_tabs .change_prd_type").click(function () {
        var change_prd_type = $(this).attr('id');
        $(".custom_tabs .change_prd_type").removeClass("admin_active");
        $(this).addClass("admin_active");
        show_data_by_type(change_prd_type);
    });

    $("#save_prc").click(function () {
        var prd_name = $("#prd_name").val();
        var prd_type = $("#prd_type").val();
        var prd_size = $("#prd_size").val();
        var prd_qty = $("#prd_qty").val();
        var prd_price = $("#prd_price").val();
        var get_ajax_url = $(".url").attr('id');
        var get_wp_prifix = $(".get_wp_prifix").attr('id');
        if (prd_name == -1) {
            alert("Please select product Name");
            $("#prd_name").focus();
        } else if (prd_type == -1) {
            alert("Please select product type");
            $("#prd_type").focus();
        } else if (prd_size == -1) {
            alert("Please select product size");
            $("#prd_size").focus();
        } else if (prd_qty == -1) {
            alert("Please select product qty.");
            $("#prd_qty").focus();
        } else if (prd_price == "") {
            alert("Please select product price");
            $("#prd_price").focus();
        } else {
            var send_data = "get_wp_prifix=" + get_wp_prifix + "&prd_name=" + prd_name + "&prd_type=" + prd_type + "&prd_size=" + prd_size + "&prd_qty=" + prd_qty + "&prd_prc=" + prd_price + "&save_price=1";
            $.ajax({
                url: get_ajax_url,
                type: 'POST',
                data: send_data,
                success: function (resp) {
                    if (resp == 1) {
                        alert("price already exists in table");
                    } else if (resp == 2) {
                        $("#prd_type").val(-1);
                        $("#prd_size").val(-1);
                        $("#prd_qty").val(-1);
                        $("#prd_price").val('');
                        var change_prd_id = $(".custom_tabs .change_prd_type.admin_active").attr('id');
                        show_data_by_type(change_prd_id);
                    } else {
                        alert(resp);
                        //alert("There is a problem to save data");
                    }
                }, error: function () {
                    //alert("There is some problem to retrieve data");
                }
            });

        }
    });

    $("body").on("click", "#itemContainer .edit_data", function () {
        var edit_id = $(this).attr('id');
        var get_ajax_url = $(".url").attr('id');
        var get_wp_prifix = $(".get_wp_prifix").attr('id');
        var update_prc_data = "update_data=1&edit_prc_id=" + edit_id + "&get_wp_prifix=" + get_wp_prifix;
        $.ajax({
            url: get_ajax_url,
            type: 'POST',
            data: update_prc_data,
            dataType: 'json',
            success: function (resp) {
                $.each(resp, function (k, v) {
                    $("#edit_prd_name").val(v.product_name).attr("selected", "selected");
                    //$("#edit_prd_type").val(v.product_type).attr("selected", "selected");
                    $("#edit_prd_name").change();
                    $("#edit_prd_size").val(v.size).attr("selected", "selected");
                    $("#edit_prd_qty").val(v.quantity).attr("selected", "selected");
                    $("#edit_prd_prc").val(v.price);
                    $("#update_pricing").attr('id', v.prc_id);
                });
            }, error: function () {
                //alert("There is some problem to retrieve data for update");
            }
        });
    });

    $("#update_pricing").click(function () {
        var update_val_id = $(this).attr('id');
        var edit_prd_name = $("#edit_prd_name").val();
        var edit_prd_type = $("#edit_prd_type").val();
        var edit_prd_size = $("#edit_prd_size").val();
        var edit_prd_qty = $("#edit_prd_qty").val();
        var edit_prd_prc = $("#edit_prd_prc").val();
        var get_ajax_url = $(".url").attr('id');
        var get_wp_prifix = $(".get_wp_prifix").attr('id');

        if (edit_prd_name == -1) {
            alert("Please select product Name");
            $("#edit_prd_type").focus();
        } else if (edit_prd_type == -1) {
            alert("Please select product Name");
            $("#edit_prd_type").focus();
        } else if (edit_prd_size == -1) {
            alert("Please select product size");
            $("#edit_prd_size").focus();
        } else if (edit_prd_qty == -1) {
            alert("Please select product qty.");
            $("#edit_prd_qty").focus();
        } else if (edit_prd_prc == "") {
            alert("Please select product price");
            $("#edit_prd_prc").focus();
        } else {
            var update_var_data = "update_prc_val=1&get_wp_prifix=" + get_wp_prifix + "&update_prd_name=" + edit_prd_name +  "&update_prd_type=" + edit_prd_type + "&update_prd_size=" + edit_prd_size + "&update_prc_qty=" + edit_prd_qty + "&update_prc_value=" + edit_prd_prc + "&update_val_id=" + update_val_id;
            $.ajax({
                url: get_ajax_url,
                type: 'POST',
                data: update_var_data,
                //dataType: 'json',
                success: function (resp) {
                    if (resp == 1) {
                        alert("price already exists in table");
                    } else if (resp == 2) {
                        $("#edit_prd_name").val(-1);
                        $("#edit_prd_type").val(-1);
                        $("#edit_prd_size").val(-1);
                        $("#edit_prd_qty").val(-1);
                        $("#edit_prd_prc").val('');
                        var change_prd_id = $(".custom_tabs .change_prd_type.admin_active").attr('id');
                        show_data_by_type(change_prd_id);
                        $('#myModal').modal('toggle');
                    } else {
                        alert("There is a problem to save data");
                    }
                }, error: function () {
                    //alert("There is some problem to update data");
                }
            });
        }
    });

    //Delete Price Value

    $("body").on("click", "#itemContainer .delete_prc_value", function () {
        var del_prc_id = $(this).attr('id');
        var get_ajax_url = $(".url").attr('id');
        var get_wp_prifix = $(".get_wp_prifix").attr('id');
        if (confirm("Are you sure you want to delete?")) {
            var delete_prc_id = "delete_price_id=1&del_prc_id=" + del_prc_id + "&get_wp_prifix=" + get_wp_prifix;
            $.ajax({
                url: get_ajax_url,
                type: 'POST',
                data: delete_prc_id,
                success: function (resp) {
                    if (resp == 1) {
                        var change_prd_id = $(".custom_tabs .change_prd_type.admin_active").attr('id');
                        show_data_by_type(change_prd_id);
                    } else {
                        alert("Error!");
                    }
                }, error: function () {
                    //alert("There is some problem to delete data");
                }
            });
        } else {
            return false;
        }
    });

    $("#reset").click(function () {
        $("#prd_type").val(-1);
        $("#prd_size").val(-1);
        $("#prd_qty").val(-1);
        $("#prd_price").val('');
    });

});


 // array of possible countries in the same order as they appear in the country selection list 
 var productTypeLists = new Array(4) 
 productTypeLists["empty"] = ["Select a Country"]; 
 productTypeLists["Embroidered_Patches"] = ["50% Embroidery", "75% Embroidery", "100% Embroidery"]; 
 productTypeLists["PVC_Patches"] = ["2D", "3D"]; 
 /* CountryChange() is called from the onchange event of a select element. 
 * param selectObj - the select object which fired the on change event. 
 */ 
 function productTypeChange(selectObj) { 
 // get the index of the selected option 
 var idx = selectObj.selectedIndex; 
 // get the value of the selected option 
 var which = selectObj.options[idx].value; 
 // use the selected option value to retrieve the list of items from the productTypeLists array 
 cList = productTypeLists[which]; 
 // get the country select element via its known id 
 var cSelect = document.getElementById("prd_type"); 
 // remove the current options from the country select 
 var len=cSelect.options.length; 
 while (cSelect.options.length > 0) { 
 cSelect.remove(0); 
 } 
 var newOption; 
 // create new options 
 for (var i=0; i<cList.length; i++) { 
 newOption = document.createElement("option"); 
 newOption.value = cList[i];  // assumes option string and value are the same 
 newOption.text=cList[i]; 
 // add the new option 
 try { 
 cSelect.add(newOption);  // this will fail in DOM browsers but is needed for IE 
 } 
 catch (e) { 
 cSelect.appendChild(newOption); 
 } 
 } 
 } 
 function editProductTypeChange(selectObj) { 
 // get the index of the selected option 
 var idx = selectObj.selectedIndex; 
 // get the value of the selected option 
 var which = selectObj.options[idx].value; 
 // use the selected option value to retrieve the list of items from the productTypeLists array 
 cList = productTypeLists[which]; 
 // get the country select element via its known id 
 var cSelect = document.getElementById("edit_prd_type"); 
 // remove the current options from the country select 
 var len=cSelect.options.length; 
 while (cSelect.options.length > 0) { 
 cSelect.remove(0); 
 } 
 var newOption; 
 // create new options 
 for (var i=0; i<cList.length; i++) { 
 newOption = document.createElement("option"); 
 newOption.value = cList[i];  // assumes option string and value are the same 
 newOption.text=cList[i]; 
 // add the new option 
 try { 
 cSelect.add(newOption);  // this will fail in DOM browsers but is needed for IE 
 } 
 catch (e) { 
 cSelect.appendChild(newOption); 
 } 
 } 
 } 
