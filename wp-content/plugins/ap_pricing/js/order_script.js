//Order Script used for light box data

jQuery(document).ready(function ($) {
    $(".btn-warning.view_data").click(function () {
        var order_id = $(this).attr('id');
        var prd_name = $(this).attr('name');        
        var get_ajax_url = $(".url").attr('id');
        var get_wp_prifix = $(".get_wp_prifix").attr('id');
        var img_url = $(".img_url").attr('id');
        var get_data = "orders_data=1&get_wp_prifix=" + get_wp_prifix + "&order_id=" + order_id + "&prd_name=" + prd_name;
        $("#myModal_3 .modal-header .modal-title").html(prd_name);
        $("#view_order_data").html('<img style="margin-left: 39%; height: 60px;" src="' + img_url + '"/>');
        $.ajax({
            url: get_ajax_url,
            type: 'POST',
            data: get_data,
            success: function (resp) {
                $("#view_order_data").html(resp);
            }, error: function () {
                //alert("There is some problem to retrieve data");
            }
        });
    });

    //Completed Product
    $(".btn-success.ordersucc_data").click(function () {
        if (confirm("Are you sure this product is complete?")) {
            var orderid = $(this).attr('id');
            var get_ajax_url = $(".url").attr('id');
            var get_wp_prifix = $(".get_wp_prifix").attr('id');
            var img_url = $(".img_url").attr('id');
            var get_data = "complete_order=1&get_wp_prifix=" + get_wp_prifix + "&order_id=" + orderid;
            $.ajax({
                url: get_ajax_url,
                type: 'POST',
                data: get_data,
                success: function (resp) {
                    if (resp != 1) {
                        alert("There is some technical problem...");
                    } else {
                        window.location.href = "";
                    }
                }, error: function () {
                    //alert("There is some problem to retrieve data");
                }
            });
        }
        return false;
    });



    $(".btn-danger.orderdel_data").click(function () {
        if (confirm("Are you sure you want to delete this order?")) {
            var del_id = $(this).attr('id');
            var get_ajax_url = $(".url").attr('id');
            var get_wp_prifix = $(".get_wp_prifix").attr('id');
            var img_url = $(".img_url").attr('id');
            var get_data = "delete_order=1&get_wp_prifix=" + get_wp_prifix + "&order_id=" + del_id;
            $.ajax({
                url: get_ajax_url,
                type: 'POST',
                data: get_data,
                success: function (resp) {
                    if (resp != 1) {
                        alert("There is some technical problem...");
                    } else {
                        window.location.href = "";
                    }
                }, error: function () {
                    //alert("There is some problem to retrieve data");
                }
            });
        }
        return false;
    });
});