<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include('connection.php');

if (isset($_POST['ret_data'])) {
    $get_wp_prifix = $_POST['get_wp_prifix'];
    global $wpdb;
    $myrows = $wpdb->get_results("SELECT * FROM $get_wp_prifix order by prc_id desc");
    $i = 1;
    foreach ($myrows as $sql_row) {
        ?>
        <tr class="hide_embrow_<?php echo $i; ?>">
            <td id="sr_no_<?php echo $i; ?>"><?php echo $i; ?></td>
            <td><?php echo $sql_row->product_name; ?></td>
            <td><?php echo $sql_row->size; ?>&rdquo;</td>
            <td><?php echo $sql_row->quantity; ?>pcs</td>
            <td>$ <?php echo $sql_row->price; ?></td>
            <td>
                <span>
                    <a data-target="#myModal" data-toggle="modal" href="javascript:void(0)" class="edit_data btn-success" id="<?php echo $sql_row->prc_id; ?>">Edit </a>
                </span>
                <span style="margin-left:15px;">
                    <a href="javascript:void(0)" class="del_data btn-danger delete_prc_value" id="<?php echo $sql_row->prc_id; ?>"> Delete</a>
                </span>
                <img id="ajax_img_<?php echo $sql_row->prc_id; ?>" src="79-contact-form/images/ajax-loader.gif" style="display: none;"/F>
            </td>
        </tr>
        <?php
        $i++;
    }
}

//Show data by product Name
if (isset($_POST['ret_data_by_name'])) {
    $prd_changed_name = $_POST['prd_changed_name'];
    $get_wp_prifix = $_POST['get_wp_prifix'];
    global $wpdb;
    if ($prd_changed_name == -1) {
        $myrows = $wpdb->get_results("SELECT * FROM $get_wp_prifix order by prc_id desc");
    } else {
        $myrows = $wpdb->get_results("SELECT * FROM $get_wp_prifix where product_name='$prd_changed_name' order by prc_id desc");
    }
    $i = 1;
    foreach ($myrows as $sql_row) {
        ?>
        <tr class="hide_embrow_1017">
            <td id="sr_no_1017"><?php echo $i; ?></td>
            <td><?php echo $sql_row->product_name; ?></td>
            <td><?php echo $sql_row->size; ?>&rdquo;</td>
            <td><?php echo $sql_row->quantity; ?>pcs</td>
            <td>$ <?php echo $sql_row->price; ?></td>
            <td>
                <span>
                    <a data-target="#myModal" data-toggle="modal" href="javascript:void(0)" class="edit_data btn-success" id="<?php echo $sql_row->prc_id; ?>">Edit </a>
                </span>
                <span style="margin-left:15px;">
                    <a href="javascript:void(0)" class="del_data btn-danger delete_prc_value" id="<?php echo $sql_row->prc_id; ?>"> Delete</a>
                </span>
                <img id="ajax_img_<?php echo $sql_row->prc_id; ?>" src="79-contact-form/images/ajax-loader.gif" style="display: none;"/F>
            </td>
        </tr>
        <?php
        $i++;
    }
	if(!$myrows){
		echo '<tr class="hide_embrow_1017"><td colspan="6" class="text-center">No product found</td></tr>';
	}
}
//Show data by product type
if (isset($_POST['ret_data_by_type'])) {
    $prd_changed_type = $_POST['prd_changed_type'];
    $get_wp_prifix = $_POST['get_wp_prifix'];
    global $wpdb;
    if ($prd_changed_type == -1) {
        $myrows = $wpdb->get_results("SELECT * FROM $get_wp_prifix order by prc_id desc");
    } else {
        $myrows = $wpdb->get_results("SELECT * FROM $get_wp_prifix where product_type='$prd_changed_type' order by prc_id desc");
    }
    $i = 1;
    foreach ($myrows as $sql_row) {
        ?>
        <tr class="hide_embrow_1017">
            <td id="sr_no_1017"><?php echo $i; ?></td>
            <td><?php echo $sql_row->product_name; ?></td>
            <td><?php echo $sql_row->size; ?>&rdquo;</td>
            <td><?php echo $sql_row->quantity; ?>pcs</td>
            <td>$ <?php echo $sql_row->price; ?></td>
            <td>
                <span>
                    <a data-target="#myModal" data-toggle="modal" href="javascript:void(0)" class="edit_data btn-success" id="<?php echo $sql_row->prc_id; ?>">Edit </a>
                </span>
                <span style="margin-left:15px;">
                    <a href="javascript:void(0)" class="del_data btn-danger delete_prc_value" id="<?php echo $sql_row->prc_id; ?>"> Delete</a>
                </span>
                <img id="ajax_img_<?php echo $sql_row->prc_id; ?>" src="79-contact-form/images/ajax-loader.gif" style="display: none;"/F>
            </td>
        </tr>
        <?php
        $i++;
    }
	if(!$myrows){
		echo '<tr class="hide_embrow_1017"><td colspan="6" class="text-center">No product found</td></tr>';
	}
}

//Save Price
if (isset($_POST['get_wp_prifix'])) {
    $prd_name = $_POST['prd_name'];
    $prd_type = $_POST['prd_type'];
    $prd_size = $_POST['prd_size'];
    $prd_qty = $_POST['prd_qty'];
    $prd_prc = $_POST['prd_prc'];
	
	$get_wp_prifix = $_POST['get_wp_prifix'];
    global $wpdb;
    $qry = "SELECT * FROM $get_wp_prifix where product_type='$prd_type' and size='$prd_size' and quantity='$prd_qty'";
    $myrows = $wpdb->get_results($qry);
    $num_rows = count($myrows); //PHP count()
    if ($num_rows == 1) {
        echo 1;
    } else {
        $datainset = $wpdb->insert($get_wp_prifix, array('product_name' => $prd_name, 'product_type' => $prd_type, 'size' => $prd_size, 'quantity' => $prd_qty, 'price' => $prd_prc));
		if ($datainset) {
            echo 2;
        }
    }
}

//Get value from database for Update Price Data
if (isset($_POST['update_data'])) {
    $edit_prc_id = $_POST['edit_prc_id'];
    $get_wp_prifix = $_POST['get_wp_prifix'];
    global $wpdb;
    $myrows = $wpdb->get_results("SELECT * FROM $get_wp_prifix where prc_id='$edit_prc_id'");
    $send_data = json_encode($myrows);
    echo $send_data;
}

//Update Price Data
if (isset($_POST['update_prc_val'])) {
    $prd_name = $_POST['update_prd_name'];
    $prd_type = $_POST['update_prd_type'];
    $prd_size = $_POST['update_prd_size'];
    $prd_qty = $_POST['update_prc_qty'];
    $prd_prc = $_POST['update_prc_value'];
    $prd_prc_id = $_POST['update_val_id'];
	
	$get_wp_prifix = $_POST['get_wp_prifix'];
    global $wpdb;
    $qry = "SELECT * FROM $get_wp_prifix where product_type='$prd_type' and size='$prd_size' and quantity='$prd_qty' and prc_id!='$prd_prc_id'";
    $myrows = $wpdb->get_results($qry);
    $num_rows = count($myrows); //PHP count()
    if ($num_rows == 1) {
        echo 1;
    } else {
        $datainset = $wpdb->update($get_wp_prifix, array('product_name' => $prd_name, 'product_type' => $prd_type, 'size' => $prd_size, 'quantity' => $prd_qty, 'price' => $prd_prc), array('prc_id' => $prd_prc_id));
        echo 2;
    }
}

if (isset($_POST['delete_price_id'])) {
    $del_prc_id = $_POST['del_prc_id'];
    $get_wp_prifix = $_POST['get_wp_prifix'];
    $delete_data = $wpdb->query("delete from $get_wp_prifix where prc_id='$del_prc_id'");
    if ($delete_data) {
        echo 1;
    } else {
        echo 2;
    }
}

?>