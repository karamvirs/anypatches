=== Plugin Name ===
Contributors: vidishp
Tags: first order discount, order discount, discount, woocommerce discount, free gift, multiple free gift
Requires at least: 3.0.1
Tested up to: 4.9.6, WooCommerce 3.4.X
Stable tag: 4.9

First Order Discount Woocommerce allows admin to offer discount to their customers on their first order with various conditions.

== Description ==

"First Order Discount Woocommerce" enables admin to offer discount to their customers on their first order with various conditions like minimum amount they need to spend to get discount or whether user can use multiple coupons together.

<strong>PRO version features:</strong>
<ul>
<li>Set minimum cart amount to get promotion</li>
<li>Show interactive shopping progress to customers (This is more likely to increase your sales)</li>
<li>Run promotion for specific category</li>
<li>Offer a range of products(simple & variable) to customers to choose from.</li>
<li>Customers can choose maximum FREE GIFTS, which can be defined by admin.</li>
</ul>
<strong>Get PRO VERSION HERE <a href="https://www.wooextend.com/product/order-promotion-woocommerce-pro/">Woocommerce Order Promotion Pro</a></strong> (With 25+ Happy Users) 

<br/>Thank you for <a href="https://www.facebook.com/wooextend/reviews">LOVING this plugin..!!</a>

<br/>Giving away small free gifts attracts most customers and increase your product sales.



<iframe width="500" height="400" src="https://www.youtube.com/embed/yaQpqcEk7pA"></iframe>


<strong>More plugins by WooExtend:</strong>
<ul>
<li><strong><a href="https://wordpress.org/plugins/woo-combo-offers/">Woocommerce Combo Offers</a></strong></li>
<li><strong><a href="https://wordpress.org/plugins/woo-product-category-discount/">Product Category Discount</a></strong></li>
<li><strong><a href="https://wordpress.org/plugins/woo-custom-fee/">Custom Fee</a></strong></li>
</ul>

== Installation ==

1. Upload `first-order-discount-woocommerce.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPresss

== Frequently Asked Questions ==

= I have coupons disabled in Woocommerce, will this plugin work? = 

No. In order for this plugin to work, you will need to turn on use of coupons.

= Why, am I not able to use "Free Product" feature? =

You can purchase our PRO version to use all features of the plugin.

== Screenshots ==

1. Admin interface where admin can configure first discount plugin.
2. Front end user can see discount applied as configured in admin.

== Changelog ==

= 1.4 =
* Fix for checkout init

= 1.3 =
* Updated coupon configurations

= 1.2 =
* Made in progress order consider for first discount.

= 1.1 =
* Released Order Promotion Woocommerce Pro.

= 1.0 =
* Initial release of plugin.