<?php
/*
Plugin Name: Anypatches Extra Data

*/

// error_reporting(E_ALL);
// ini_set('display_errors',1);

/*Added by Karam*/
include('connection.php');

add_action( 'woocommerce_before_calculate_totals', 'before_calculate_totals_cstm', 10, 1 );
add_filter( 'woocommerce_add_cart_item_data', 'add_cart_item_data_cstm', 10, 3 );

// add_action( 'woocommerce_after_calculate_totals', array( $this, 'hide_checkout_button' ), 10, 1 );

add_filter( 'woocommerce_billing_fields', 'make_fields_non_required', 10, 1 );
// Remove the word 'Billing' from validation errors
add_filter( 'woocommerce_checkout_required_field_notice', 'filter_woocommerce_checkout_required_field_notice', 10, 2 );
function filter_woocommerce_checkout_required_field_notice( $sprintf, $field_label ) { 
    return str_replace('Billing ','',$sprintf); 
}; 
         

//add_action('woocommerce_checkout_process', 'email_check');
add_action('woocommerce_review_order_after_cart_contents', 'email_check');


//add_action('woocommerce_review_order_after_order_total','email_check',10);

function email_check() {
    
    global $wpdb;
    
    $post = explode('&',urldecode($_POST['post_data']));
    
    $email_string = $post[14];
    $email = explode('=',$email_string)[1];
    
    
    if(!$email)
        return;
        

    if(fodw_has_bought_new($email)){
        //file_put_contents('/home/anypatches/public_html/post.txt',print_r(WC()->cart->applied_coupons,true));
        if(isset(WC()->cart->applied_coupons) && !empty(WC()->cart->applied_coupons)) {
     	    if(in_array('first order discount',WC()->cart->applied_coupons)){
     	        
     	        WC()->cart->remove_coupon( 'first order discount' );
     	    }
     		
     	}
        return;
    }

    $productInCart = false;
    foreach( WC()->cart->get_cart() as $cart_item_key => $values ) {
		$_product = $values['data'];
	
		if( $arrData['freeProduct'] == $_product->get_id() ) {
			$productInCart = true;
		}
	}

 	// if coupon already applied
 	if(isset(WC()->cart->applied_coupons) && !empty(WC()->cart->applied_coupons) && in_array('first order discount',WC()->cart->applied_coupons)) {
 	    return;
 	}

	
    WC()->cart->add_discount( 'first order discount' );
 
}

function fodw_has_bought_new($email) {
        
    $count = 0;
    $bought = false;

    // Get all customer orders
    $conditions = array(
        
        'meta_key'    => '_billing_email',
        'meta_value'  => "$email",
        'post_type'   => 'shop_order', // WC orders post type
        'post_status' => array('wc-completed', 'wc-in-progress', 'in-progress','wc-processing','wc-on-hold') // Only orders with status "completed" & "In  Progress"
    ) ;
    $customer_orders = get_posts( $conditions);
//file_put_contents('/home/anypatches/public_html/post.txt',print_r($customer_orders,true));

    // Going through each current customer orders
    foreach ( $customer_orders as $customer_order ) {
        $count++;
    }

    // return "true" when customer has already one order
    if ( $count > 0 ) {
        $bought = true;
    }
    return $bought;
}         
         
         
add_action('woocommerce_process_shop_order_meta', 'send_tracking_email', 0, 1);

function send_tracking_email($post_id, $post)
            {
                $to = $_POST['_billing_email'];
                $subject = 'Tracking info for your order';
                $from = 'admin@anypatches.com';
                $headers  = 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                 
                // Create email headers
                $headers .= 'From: '.$from."\r\n".
                    'Reply-To: '.$from."\r\n" .
                    'X-Mailer: PHP/' . phpversion();
                    
                //echo "<pre>";print_r($_POST);die;
                $body = 'Hi<br>
                        <p>Here is the tracking info for your order #'.$_POST['post_ID'].'</p>
                        <p>Carrier:'.$_POST['aftership_tracking_provider_name'].'</p>
                        <p>Tracking number:'.$_POST['aftership_tracking_number'].'</p>
                        <p>&nbsp;</p>
                        <p>Thanks</p>
                        <p>Anypatches Admin</p>';
                mail($to,$subject,$body,$headers);
                
            }


function make_fields_non_required( $address_fields ) {
    //  echo "<pre>";print_r($address_fields);die;
	$address_fields['billing_phone']['required'] = false;
	$address_fields['billing_company']['required'] = false;
	$address_fields['billing_country']['required'] = false;
	$address_fields['billing_address_1']['required'] = false;
	$address_fields['billing_address_2']['required'] = false;
	$address_fields['billing_city']['required'] = false;
	$address_fields['billing_state']['required'] = false;
	$address_fields['billing_postcode']['required'] = false;
	
		return $address_fields;
}

//for displaying pricing on product pages
add_shortcode( 'option_prices', 'option_prices' );
add_shortcode( 'option_prices', 'option_prices' );



function option_prices($atts){
	
    $product_id = $atts['pid'];
    show_option_prices($product_id);
}


function show_option_prices($product_id){
    global $wpdb;
    $results = [];

    switch($product_id){
        case 228:
            $so_post_id = 241;
            $product_name = 'Embroidered_Patches';
            $results[] = $wpdb->get_results("SELECT * FROM ap_pricing where product_name = '$product_name' AND product_type = '50% Embroidery' LIMIT 1");
            $results[] = $wpdb->get_results("SELECT * FROM ap_pricing where product_name = '$product_name' AND product_type = '75% Embroidery'  LIMIT 1");
            $results[] = $wpdb->get_results("SELECT * FROM ap_pricing where product_name = '$product_name' AND product_type = '100% Embroidery' LIMIT 1");

            
            
        break;
        case 229:
            $so_post_id = 244;
            $product_name = 'Woven_Patches';
            $results[] = $wpdb->get_results("SELECT * FROM ap_pricing where product_name = '$product_name' LIMIT 1");
            
            
        break;
        case 230:
        $product_name = 'Chenille_Patches';
            $so_post_id = 245;
            $results[] = $wpdb->get_results("SELECT * FROM ap_pricing where product_name = '$product_name' LIMIT 1");
            
        break;
        case 231:
            $so_post_id = 243;   
            $product_name = 'PVC_Patches';
            $results[] = $wpdb->get_results("SELECT * FROM ap_pricing where product_name = '$product_name' AND product_type = '2D' LIMIT 1");
            $results[] = $wpdb->get_results("SELECT * FROM ap_pricing where product_name = '$product_name' AND product_type = '3D' LIMIT 1");
            
            
        break;
        
    }
    $html = '';
    
    $quantities = $wpdb->get_results("SELECT DISTINCT(quantity) FROM ap_pricing  WHERE product_name = '$product_name' order by quantity");
    $sizes = $wpdb->get_results("SELECT DISTINCT(size) FROM ap_pricing  WHERE product_name = '$product_name' order by size");

    // echo "<pre>";print_r($results);die;

    
    $html .= '<ul class="nav nav-tabs" role="tablist">';
    $loop = 0;
         foreach($results as $result){//echo "<pre>";print_r($result[0]->product_name);die;
            $active = $loop===0?'active':'';
            $title = count($results)==1?str_replace('_',' ',$result[0]->product_name):$result[0]->product_type;
            $attr = str_replace([' ','%'],['-',''],$result[0]->product_type);
                $html .= '<li class="'.$active.'" role="presentation"><a role="tab" href="#'.$attr.'" aria-controls="'.$attr.'" data-toggle="tab">'.$title.'</a></li>';
            $loop++;
            }
            
    $html .= '</ul><div class="tab-content">';
    
    $loop = 0;
    foreach($results as $result){
        $active = $loop===0?'active':'';
        $attr = str_replace([' ','%'],['-',''],$result[0]->product_type);
        $html .= '<div id="'.$attr.'" class="tab-pane '.$active.'" role="tabpanel">
        <table class="table table-striped table-hover">
        <thead>
        <tr>
        <th>Size</th>';
        
        foreach($quantities as $quantity){
            
            $html .= '<th>'.$quantity->quantity.'</th>';
    
        }
        $html .= '</tr>
        </thead>
        <tbody>
        ';
        foreach($sizes as $size){
            $html .= '<tr><td>'.$size->size.'</td>';
            foreach($quantities as $quantity){
                $price = $wpdb->get_col("SELECT price FROM ap_pricing  WHERE product_name = '$product_name' AND product_type = '".$result[0]->product_type."' AND quantity = ".$quantity->quantity." AND size =  ".$size->size." LIMIT 1");
                $price = $price?'$'.$price[0]:'N/A';
                $html .= '<td>'.$price.'</td>';
        
            }
            $html .= '</tr>';
        }

        $html .= '</tbody>
        </table>
        </div>';
        $loop++;
    }
    
    $html .= '</div>';
    
echo $html;
    // return $html;
    /*
    $so_query = "SELECT meta_value FROM ap_postmeta WHERE post_id = '$so_post_id' AND meta_key = '_product_addons'";
    $special_options = unserialize($wpdb->get_col($so_query)[0])[0]['options'];
     //echo "here<pre>";print_r($special_options);die;
    $so_names = array_column($special_options,'label');
    $so_prices = array_column($special_options,'price');*/

}
function before_calculate_totals_cstm( $cart_obj ) {
    
   
  if ( is_admin() && ! defined( 'DOING_AJAX' ) ) {
    return;
  }

  $cart = $cart_obj->get_cart();

  foreach( $cart as $key=>$value ) {

        if( isset( $value['final_price'] ) && strpos($_SERVER['HTTP_REFERER'],'cart')==false) {
            
            //This is the case when user comes from the product page
            $price = $value['final_price'];
            $value['data']->set_price( ( $price ) );
         
        }else{
            $return = calculate_prices($value['product_id'],$value);
            $price = $return['final_price'];
            
            $value['data']->set_price( $price );
            $value['final_price'] = $price;
            
            
            $cart[$key]['final_price'] = $price;
            
        }
   
  }
   $cart_obj->set_cart_contents($cart);



}

function add_cart_item_data_cstm( $cart_item_data, $product_id, $variation_id ){
    
     /* echo "<pre>";print_r($_POST);
     print_r($cart_item_data);
     die; */
    $cart_item_data = calculate_prices($product_id,$_POST);
    return $cart_item_data;
}



function calculate_prices($product_id, $post ) {

    
    global $wpdb;
    switch($product_id){
        case 228:
            $so_post_id = 241;
            $so_array_key = 'addon-228-special-options-2';
            $product_name = 'Embroidered_Patches';
            $product_type_key = 'addon-228-product-type-1';
        break;
        case 229:
            $so_post_id = 244;
            $so_array_key = 'addon-229-special-options-1';
            $product_name = 'Woven_Patches';
        break;
        case 230:
            $so_post_id = 245;
            $so_array_key = 'addon-230-special-options-1';
            $product_name = 'Chenille_Patches';
        break;
        case 231:
            $so_post_id = 243;   
            $so_array_key = 'addon-231-special-options-2';
            $product_name = 'PVC_Patches';  
            $product_type_key = 'addon-231-product-type-1';
        break;
        
    }

 $product_type = '';

 if( isset( $post['addon-'.$product_id.'-dimensions-0']['height'] ) && $post['addon-'.$product_id.'-dimensions-0']['height']) {
      $ap_height = $post['addon-'.$product_id.'-dimensions-0']['height'];
    }
    if( isset( $post['addon-'.$product_id.'-dimensions-0']['width'] ) && $post['addon-'.$product_id.'-dimensions-0']['width']) {
      $ap_width = $post['addon-'.$product_id.'-dimensions-0']['width'];
    }
//   file_put_contents('/home/anypatches/public_html/post.txt', print_r($post,true));
  
    //check for units
    switch($post['addon-230-units-3'][0]){
        
        case 'in-1':
        default:
            
        break;
        
        case 'cm-2':
            $ap_height = $ap_height/2.5;
            $ap_width = $ap_width/2.5;
        break;
        
        case 'mm-3':
            $ap_height = $ap_height/25;
            $ap_width = $ap_width/25;
        break;
            
    }
  
//   file_put_contents('/home/anypatches/public_html/post.txt', 'herer::'.$ap_height.' || '.$ap_width);
  
    $product = wc_get_product( $product_id );
    
     $price = $product->get_price();
  
    switch($product_id){
        case 228:
            if(isset($post[$product_type_key]) && $post[$product_type_key] == '50-embroidery-1'){$product_type = "50% Embroidery";}
            if(isset($post[$product_type_key]) && $post[$product_type_key] == '75-embroidery-2'){$product_type = "75% Embroidery";}
            if(isset($post[$product_type_key]) && $post[$product_type_key] == '100-embroidery-3'){$product_type = "100% Embroidery";}
        break;
        case 231:
            if(isset($post[$product_type_key]) && $post[$product_type_key] == '2d-1'){$product_type = "2D";}
            if(isset($post[$product_type_key]) && $post[$product_type_key] == '3d-2'){$product_type = "3D";}
            
        break;
    }
    
    $product_type_query = $product_type!='' ? " and product_type = '$product_type' ":'';
    
    $ap_quantity = $post['quantity'];
    
    if($ap_quantity < MIN_QUANTITY)
        $ap_quantity = MIN_QUANTITY;

    $sql = "SELECT DISTINCT(quantity) as added_qty FROM `".$wpdb->prefix."pricing` where product_name='$product_name' $product_type_query and quantity = '$ap_quantity' LIMIT 1";
    
	$quantity_qry = $wpdb->get_results($sql);
    $count_qty = $wpdb->num_rows;
    if ($count_qty != 1) {
        $sql = "SELECT (SELECT DISTINCT(quantity) FROM `".$wpdb->prefix."pricing` where product_name='$product_name' $product_type_query and quantity < '$ap_quantity' ORDER by quantity DESC LIMIT 1) as min_qty,(SELECT DISTINCT(quantity) FROM `".$wpdb->prefix."pricing` where product_name='$product_name' $product_type_query and quantity > '$ap_quantity' ORDER by quantity ASC LIMIT 1) as max_qty";

        $quantity_qry = $wpdb->get_results($sql);
    }
    $get_qty_db = $quantity_qry[0]->added_qty;


    //users enterd quantity if quantity exists in database
    if ($get_qty_db != "" && $get_qty_db != null) {
        $ap_quantity = $get_qty_db;
        //if users entered float value in height and width boxes
        if (strpos($ap_width, '.') !== false || strpos($ap_height, '.') !== false) {
            $patch_size = ($ap_height + $ap_width) / 2;
            $patch_size1 = number_format($patch_size, 2, '.', '');
            $explode_size = explode(".", $patch_size1);
            $point_val = "." . $explode_size[1];

            //height+width/2= if we get answer float value
            if ($point_val > 0 && $point_val <= 0.12) {
                $a = "0";
            } else if ($point_val >= 0.13 && $point_val <= 0.25) {
                $b = "0.25";
            } else if ($point_val > 0.25 && $point_val <= 0.37) {
                $b = "0.25";
            } else if ($point_val >= 0.38 && $point_val <= 0.50) {
                $a = "0.5";
            } else if ($point_val > 0.50 && $point_val <= 0.62) {
                $a = "0.5";
            } else if ($point_val >= 0.63 && $point_val <= 0.75) {
                $b = "0.75";
            } else if ($point_val > 0.75 && $point_val <= 0.87) {
                $b = "0.75";
            } else if ($point_val >= 0.88 && $point_val <= 1.00) {
                $a = "1";
            }
            if ($b != "") {

                //height+width/2= if we get answer float value and its value not not matching in server price
                

                $next_number = floorToFraction($patch_size, 2);

                //Get Min Price from server                
                $quantity_qry = $wpdb->get_results("SELECT MIN(size) as server_min_size FROM `".$wpdb->prefix."pricing` where size !='0' and product_name='$product_name' $product_type_query");
                $server_min_price = $quantity_qry[0]->server_min_size;
                if ($next_number < $server_min_price) {
                    $next_number = $server_min_price;
                }
                $prev_number = $next_number + 0.5;
                $get_exact_price = $wpdb->get_results("select prc_id,price FROM `".$wpdb->prefix."pricing` where product_name='$product_name' $product_type_query and quantity='$get_qty_db' and size BETWEEN '$next_number' AND '$prev_number'");
                foreach ($get_exact_price as $fetch_price) {
                    $server_price[] = $fetch_price->price;
                }
                $server_price_min = ($server_price[0] + $server_price[1]) / 2;
                $getmain_price_withcalculation = $server_price_min * $get_qty_db;
                //file_put_contents('/home/aa29i5iet7ur/public_html/anypatches/post.txt','1:'.$getmain_price_withcalculation.' | '.$ap_quantity.' | '.$server_price_min);
                $org_mul_prc = $getmain_price_withcalculation/$get_qty_db;
            } else {
                //height+width/2= if we get answer float value and its value matching in server price
                $point_val = $explode_size[0] + $a;
                $exact_size = $point_val;
                
                $fetch_min_server_price = $wpdb->get_results("SELECT MIN(size) as server_min_size FROM `".$wpdb->prefix."pricing` where size !='0' and  product_name='$product_name' $product_type_query ");
                $server_min_price = $fetch_min_server_price[0]->server_min_size;
                if ($exact_size < $server_min_price) {
                    $exact_size = $server_min_price;
                }
                $tested_query = $wpdb->get_results("select price as main_price FROM `".$wpdb->prefix."pricing` where product_name='$product_name' $product_type_query and size='$exact_size' and quantity='$get_qty_db'");
                $main_price = $tested_query[0]->main_price;
                $getmain_price_withcalculation = $main_price * $get_qty_db;
                
                //file_put_contents('/home/aa29i5iet7ur/public_html/anypatches/post.txt',$getmain_price_withcalculation);die;
                $org_mul_prc = $getmain_price_withcalculation / $get_qty_db;
            }
        } else {
            
            //if users entered height and width without float value
            $patch_size = ($ap_height + $ap_width) / 2;
            $exact_size = floor($patch_size * 2) / 2;
            $sql = "select price FROM `".$wpdb->prefix."pricing` where product_name='$product_name' $product_type_query and size='$exact_size' and quantity='$ap_quantity'";
            
            $tested_query = $wpdb->get_results($sql);
            $get_price_db = $tested_query[0]->price;
            $getmain_price_withcalculation = $get_price_db * $get_qty_db;
            
            //file_put_contents('/home/aa29i5iet7ur/public_html/anypatches/post.txt','2:'.$getmain_price_withcalculation.' | '.$ap_quantity.' | '.$org_mul_prc);
            $org_mul_prc = $getmain_price_withcalculation / $get_qty_db;
        }
    } else {
        //if user quantity is not found in db, get nearest lower and upper quantities
        $get_min_qty = $quantity_qry[0]->min_qty;
        $get_max_qty = $quantity_qry[0]->max_qty;

        //if users entered float value in height and width boxes
        if (strpos($ap_width, '.') !== false || strpos($ap_height, '.') !== false) {
            //height+width/2= if we get answer float value and its value not not matching in server price

            $patch_size = ($ap_height + $ap_width) / 2;
            $patch_size1 = number_format($patch_size, 2, '.', '');
            $explode_size = explode(".", $patch_size1);
            $point_val = "." . $explode_size[1];

            // die($patch_size .'|| '.$patch_size1 . ' || ' .$point_val  );
            if ($point_val > 0 && $point_val <= 0.12) {
                $a = "0";
            } else if ($point_val >= 0.13 && $point_val <= 0.25) {
                $b = "0.25";
            } else if ($point_val > 0.25 && $point_val <= 0.37) {
                $b = "0.25";
            } else if ($point_val >= 0.38 && $point_val <= 0.50) {
                $a = "0.5";
            } else if ($point_val > 0.50 && $point_val <= 0.62) {
                $a = "0.5";
            } else if ($point_val >= 0.63 && $point_val <= 0.75) {
                $b = "0.75";
            } else if ($point_val > 0.75 && $point_val <= 0.87) {
                $b = "0.75";
            } else if ($point_val >= 0.88 && $point_val <= 1.00) {
                $a = "1";
            }

            if ($b != "") {

                $next_number = floorToFraction($patch_size, 2);

                $run_min_server_price = $wpdb->get_results("SELECT MIN(size) as server_min_size FROM `".$wpdb->prefix."pricing` where size !='0'and product_name='$product_name' $product_type_query ");
                $server_min_price = $run_min_server_price[0]->server_min_size;
                if ($next_number < $server_min_price) {
                    $next_number = $server_min_price;
                }
                $prev_number = $next_number + 0.5;
                $get_exact_price = $wpdb->get_results("select prc_id,price FROM `".$wpdb->prefix."pricing` where product_name='$product_name' $product_type_query and quantity='$get_min_qty' and size BETWEEN '$next_number' AND '$prev_number'");
                $get_exact_price1 = $wpdb->get_results("select prc_id,price FROM `".$wpdb->prefix."pricing` where product_name='$product_name' $product_type_query and quantity='$get_max_qty' and size BETWEEN '$next_number' AND '$prev_number'");
                foreach ($get_exact_price as $tested_query) {
                    $server_price[] = $tested_query->price;
                }
                foreach ($get_exact_price1 as $tested_query1) {
                    $server_price1[] = $tested_query1->price;
                }
                $server_price_min = ($server_price[0] + $server_price[1]) / 2;
                $server_price_max = ($server_price1[0] + $server_price1[1]) / 2;
                $qty_devide = ($ap_quantity - $get_min_qty) / 100;
                $total_min_price = $server_price_min * $get_min_qty;
                $total_max_price = $server_price_max * $get_max_qty;
                $total_price_with_qty = $total_max_price - $total_min_price;
                $devide_qty_devide = $total_price_with_qty * $qty_devide;
                $getmain_price_withcalculation = $total_min_price + $devide_qty_devide;
                //file_put_contents('/home/aa29i5iet7ur/public_html/anypatches/post.txt','3:'.$getmain_price_withcalculation.' | '.$ap_quantity.' | '.$org_mul_prc);
                $org_mul_prc = $getmain_price_withcalculation / $ap_quantity;
            } else {
                //height+width/2= if we get answer float value and its value not not matching in server price
                $point_val = $explode_size[0] + $a;
                $exact_size = $point_val;
                
                $run_min_server_price = $wpdb->get_results("SELECT MIN(size) as server_min_size FROM `".$wpdb->prefix."pricing` where size !='0' and product_name='$product_name' $product_type_query ");
                $server_min_price = $run_min_server_price[0]->server_min_size;
                if ($exact_size < $server_min_price) {
                    $exact_size = $server_min_price;
                }


                //$sql = "SELECT (select price FROM `".$wpdb->prefix."pricing` where product_name='Embroidered_Patches' and product_type = '$product_type' and size='$exact_size' and quantity='$get_min_qty') as min_price,(select price FROM `".$wpdb->prefix."pricing` where product_name='Embroidered_Patches' and product_type = '$product_type' and size='$exact_size' and quantity='$get_max_qty') as max_price";

                $sql = "SELECT * FROM `".$wpdb->prefix."pricing` WHERE product_name='$product_name' $product_type_query and size='$exact_size' and (quantity = $get_min_qty OR quantity = $get_max_qty) ORDER BY quantity ASC";

                $tested_query = $wpdb->get_results($sql);

                $min_price = $tested_query[0]->price;
                $max_price = $tested_query[1]->price;

                $qty_devide = ($ap_quantity - $get_min_qty) / 100;
                $total_min_price = $min_price * $get_min_qty;
                $total_max_price = $max_price * $get_max_qty;
                $total_price_with_qty = $total_max_price - $total_min_price;
                $devide_qty_devide = $total_price_with_qty * $qty_devide;
                $getmain_price_withcalculation = $total_min_price + $devide_qty_devide;
                //file_put_contents('/home/aa29i5iet7ur/public_html/anypatches/post.txt','4:'.$getmain_price_withcalculation.' | '.$ap_quantity.' | '.$org_mul_prc);
                $org_mul_prc = $getmain_price_withcalculation / $ap_quantity;//get the unit price after all calculations
                
            }
            // die($ap_quantity.'||'.$total_min_price.'||'.$total_max_price.'||'.$total_price_with_qty.'||'.$qty_devide.'||'.$devide_qty_devide.'||'.$getmain_price_withcalculation.'||'.$org_mul_prc);
        } else {
            //if users entered height and width without float value
            $patch_size = ($ap_height + $ap_width) / 2;
            $exact_size = floor($patch_size * 2) / 2;
           

            //Check if the user patch size is less than the min size for this product in database.If yes, then assume the user size = min db size
            $run_min_server_price = $wpdb->get_results("SELECT MIN(size) as server_min_size FROM `".$wpdb->prefix."pricing` where size !='0' and product_name='$product_name' $product_type_query ");
            
            $server_min_price = $run_min_server_price[0]->server_min_size;
            if ($exact_size < $server_min_price) {
                $exact_size = $server_min_price;
            }
            
            //Get the prices of the nearest upper and lower quantities in DB 
            $sql = "SELECT * FROM `".$wpdb->prefix."pricing` WHERE product_name='$product_name' $product_type_query and size='$exact_size' and (quantity = $get_min_qty OR quantity = $get_max_qty) ORDER BY quantity ASC";
            //$sql = "SELECT (select price FROM `".$wpdb->prefix."pricing` where product_name='Embroidered_Patches' and product_type = '$product_type' and size='$exact_size' and quantity='$get_min_qty') as min_price,(select price FROM `".$wpdb->prefix."pricing` where product_name='Embroidered_Patches' and product_type = '$product_type' and size='$exact_size' and quantity='$get_max_qty') as max_price";

            $get_exact_price = $wpdb->get_results($sql);
// echo "<pre>";var_dump($get_exact_price);die;
            $min_price = $get_exact_price[0]->price;
            $max_price = $get_exact_price[1]->price;
            //   die($min_price.' || '.$max_price);
            $qty_devide = ($ap_quantity - $get_min_qty) / 100;
            $total_min_price = $min_price * $get_min_qty;
            $total_max_price = $max_price * $get_max_qty;
            $total_price_with_qty = $total_max_price - $total_min_price;
            $devide_qty_devide = $total_price_with_qty * $qty_devide;
            $getmain_price_withcalculation = $total_min_price + $devide_qty_devide;
            $org_mul_prc = $getmain_price_withcalculation/$ap_quantity; //get the unit price after all calculations

//file_put_contents('/home/aa29i5iet7ur/public_html/anypatches/post.txt',$getmain_price_withcalculation.' | '.$ap_quantity.' | '.$org_mul_prc);

            
        }
    }
    //$org_unit_price = $org_mul_prc / $ap_quantity;
    $org_unit_price = $org_mul_prc ;

	$merrowed_border_1 = $adhesive_backing_2 = $button_loop_3 = $frayed_edges_4 = $embroidery_with_glow_in_the_dark_thread_5 = $embroidery_with_glow_in_the_dark_thread_6 = $magnetic_patch_7 = $pin_patches_8 = $reflective_9 = $velcro_patch_10 = $iron_on_backing_11 = $velcro_backing_12 = $camouflage_13 = $gingham_line_14 = $embroidery_with_glow_in_the_dark_thread_15 = $magnetic_backing_16 = $metallic_thread_17 = $puff_patches_18 = $vintage_worn_style_19 = 0 ;


$so_query = "SELECT meta_value FROM ap_postmeta WHERE post_id = '$so_post_id' AND meta_key = '_product_addons'";
$special_options = unserialize($wpdb->get_col($so_query)[0])[0]['options'];
$so_names = array_column($special_options,'label');

$loop = 0;
foreach($so_names as $k=>$v){
    $loop++;
    $so_names[$k] = str_replace(' ','-',strtolower($v)).'-'.$loop;
}
$so_prices = array_column($special_options,'price');
   

 foreach ( $post[$so_array_key] as $special_option ){
        $key = array_search(trim($special_option), $so_names);
        $so_price = $key!==false?$so_prices[$key]:0;
        if(strpos($so_price,'.') !== false){
            //This is a fixed price
            $getmain_price_withcalculation += $so_price*$ap_quantity;
        }else{
            //this is a percentage price
            $getmain_price_withcalculation += $getmain_price_withcalculation*$so_price/100;
        }
        
        
	}
    // die;
    file_put_contents('/home/aa29i5iet7ur/public_html/anypatches/post.txt','final::'.$getmain_price_withcalculation.' || '.$ap_quantity);
    // die($getmain_price_withcalculation);
    $return['final_price'] = round($getmain_price_withcalculation/$ap_quantity,2);
    $return[$so_array_key] = $post[$so_array_key];
    $return['addon-'.$product_id.'-dimensions-0'] = $post['addon-'.$product_id.'-dimensions-0'];
    if(isset($product_type_key)){
        $return[$product_type_key] = $post[$product_type_key];
    }
   $return['final_price'] = .01;
//   echo "<pre>";print_r($return);die;
 return $return;
}

function floorToFraction($number, $denominator = 1) {
    $x = $number * $denominator;
    $x = floor($x);
    $x = $x / $denominator;
    return $x;
}

add_action( 'woocommerce_after_single_product', 'action_woocommerce_after_single_product', 10, 0 ); 

function action_woocommerce_after_single_product(){
?>

<script type="text/javascript">
window.onload = function(){console.log('here');
    //console.log(jQuery('.product-addon-units select')[0].before('<label>Units</label>').wrap('<p class="form-row form-row-wide addon-wrap-230-dimensions-0"></p>'));
    // jQuery('.product-addon-dimensions').after('<p class="form-row form-row-wide addon-wrap-230-dimensions-0"><label>Units</label>'+jQuery('.product-addon-units select')[0]+'</p>');
    jQuery('.addon-wrap-230-units-3,.addon-wrap-228-units-4,.addon-wrap-231-units-4,.addon-wrap-229-units-3').insertBefore('.product-addon-dimensions .clear').prepend('<lable>Units</label>');
    jQuery('.product-addon-units').detach();
    
    jQuery('.quantity').insertBefore('.product-addon-special-options');
}
   
</script>

<?php

}
