<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteenc
 * @since Twenty Sixteen 1.0
 */

?>

		</div><!-- .site-content -->

		<a class="uparrow" id="uparrow" style="display: block;"><i class="ion-ios-arrow-up"></i></a>
		
		<footer id="colophon" class="footer" role="contentinfo">
		    <div class="container">
		      <div class="row">
		      <div class="col-md-3 col-sm-6 col-xs-12 padd_right_0 "><?php dynamic_sidebar('footer-1'); ?></div>
		      <div class="col-md-3 col-sm-6 col-xs-12"><?php dynamic_sidebar('footer-2'); ?></div>
		      <div class="col-md-3 col-sm-6 col-xs-12 padd_left_0"><?php dynamic_sidebar('footer-3'); ?></div>
		      <div class="col-md-3 col-sm-6 col-xs-12 padd_right_0"><?php dynamic_sidebar('footer-4'); ?></div>
			  
			   </div>
			
			  
			

			<?php if ( has_nav_menu( 'social' ) ) : ?>
				<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Social Links Menu', 'twentysixteen' ); ?>">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'social',
							'menu_class'     => 'social-links-menu',
							'depth'          => 1,
							'link_before'    => '<span class="screen-reader-text">',
							'link_after'     => '</span>',
						) );
					?>
				</nav><!-- .social-navigation -->
			<?php endif; ?>

			
			</div><!-- .container -->
		</footer><!-- .site-footer -->
		
		
<div class="copyright">
<div class="container">
				
			<?php dynamic_sidebar('copyright-area')?>
			
			</div><!-- .container-->	
			</div><!-- .site-info -->	
		
		
	</div><!-- .site-inner -->
	
	
<!-- contact-popup -->
<div class="modal fade contact-popup fancy-popup" id="contact-popup" tabindex="-1" role="dialog" aria-labelledby="contact-popupLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header section-title">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion-close"></i></button>
        <h3 class="modal-title" id="contact-popupLabel">Contact Us</h3>
      </div>
      <div class="modal-body">
       <?php echo do_shortcode('[contact-form-7 id="267" title="contact-page-form"]');?>
      </div>
    </div>
  </div>
</div>
	

<!-- get-quote-popup -->
<div class="modal fade get-quote-popup fancy-popup" id="get-quote-popup" tabindex="-1" role="dialog" aria-labelledby="get-quote-popupLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header section-title">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="ion-close"></i></button>
        <h3 class="modal-title" id="get-quote-popupLabel">Get a Free Quote</h3>
      </div>
      <div class="modal-body">
       <?php echo do_shortcode('[contact-form-7 id="435" title="quote-form-popup"]');?>
      </div>
    </div>
  </div>
</div>
	
	
	
	
	
	
	
	
	

<?php wp_footer(); ?>

<script type='text/javascript'>
window.onload = function(){
    // console.log('here');
    jQuery('.testimonial-free').wrapInner("<a href='<?php echo site_url();  ?>/reviews/'></a>");
}
	
document.addEventListener( 'wpcf7submit', function( event ) {
    if ( '232' == event.detail.contactFormId ) {
      
      var ap_products =  document.getElementById("ap-products").value;

      if (ap_products == "Embroidered  Patches") {
        location = "<?php echo site_url().'/product/embroidered-patches/'; ?>";
      } else if (ap_products == "Woven Patches") {
        location = "<?php echo site_url().'/anypatches/product/woven-patches/'; ?>";
      } else if (ap_products == "PVC Patches") {
        location = "<?php echo site_url().'/anypatches/product/pvc-patches/'; ?>";
      } else if (ap_products == "Chenille Patches") {
        location = "<?php echo site_url().'/anypatches/product/chenille-patches/'; ?>";
      }
      
    }
}, false )  
</script>

<script src="<?php bloginfo('template_url'); ?>/chosen/prism.js" type='text/javascript'></script>
<script src="<?php bloginfo('template_url'); ?>/chosen/init.js" type='text/javascript'></script>

<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" type='text/javascript'></script>
<script src="<?php bloginfo('template_url'); ?>/js/custom.js" type='text/javascript'></script>

<script data-skip-moving="true">
        (function(w,d,u){
                var s=d.createElement('script');s.async=1;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.com/b7732091/crm/site_button/loader_1_ykq7qa.js');
</script>


</body>
</html>
