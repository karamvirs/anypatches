<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800|Poppins:100,200,300,400,500,600,700,800" rel="stylesheet">
	<?php wp_head(); ?>
	<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<!--link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/animated.css" type="text/css" media="screen" /-->
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" type="text/css" media="screen" />	
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/responsive.css" type="text/css">
	<?php
wp_enqueue_style('chosen-css',get_template_directory_uri()."/chosen/chosen.css?ver=".rand(99,999));

if(is_product()){ ?>
	<script src="<?php bloginfo('template_url'); ?>/js/jquery-ui.min.js"></script>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/jquery-ui.min.css">
	<script type="text/javascript">
	// $.noConflict();
	jQuery( document ).ready(function(  ) {
		jQuery( ".addon-wrap-231-date-needed-3 input,.addon-wrap-228-date-needed-3 input, .addon-wrap-229-date-needed-2 input, .addon-wrap-230-date-needed-2 input" ).datepicker({
			minDate : 0
		});
	});
	
	</script>
  <?php } ?>



<script src="<?php bloginfo('template_url'); ?>/chosen/chosen.jquery.js" type='text/javascript'></script>

</head>

<body <?php body_class(); ?>>
<div class="overlayer"></div>
<div id="page" class="main_wrapper">
	<!-- ====================Header =====================-->

<header class="navbar navbar-static-top header" id="top" role="slider">
	<div class="container">

	<div class="navbar-header">
	
	<?php  if(is_front_page()) 
	{
	 twentysixteen_the_custom_logo();
	}
	else { 
      twentysixteen_the_custom_logo();

	} 
	?>
	
	
	</div>

	 <!---button type="button" class="navbar-toggle" id="navbar-toggle">
	<div class="bars"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div>
		</button-->
		
	
				
				<?php if ( has_nav_menu( 'primary' ) || has_nav_menu( 'social' ) ) : ?>

						<?php if ( has_nav_menu( 'primary' ) ) : ?>
						
							<nav class="main-navbar" id="main-navbar" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
								<?php
									wp_nav_menu( array(
										'theme_location' => 'primary',
										'menu_class'     => 'primary-menu main-menu nav navbar-nav navbar-right',
									 ) );
								?>
							</nav><!-- .main-navigation -->
							
						<?php endif; ?>

						<?php if ( has_nav_menu( 'social' ) ) : ?>
							<nav id="social-navigation" class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'twentysixteen' ); ?>">
								<?php
									wp_nav_menu( array(
										'theme_location' => 'social',
										'menu_class'     => 'social-links-menu',
										'depth'          => 1,
										'link_before'    => '<span class="screen-reader-text">',
										'link_after'     => '</span>',
									) );
								?>
							</nav><!-- .social-navigation -->
						<?php endif; ?>
			
				<?php endif; ?>
	</div>
</header>

<aside class="sidebar">
<button type="button" class="navbar-toggle" id="navbar-toggle">
	<div class="bars"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div>
		</button>		
<?php if ( has_nav_menu( 'Sidebar' ) || has_nav_menu( 'social' ) ) : ?>

						<?php if ( has_nav_menu( 'Sidebar' ) ) : ?>
						
							<nav class="main-navbar" id="main-navbar" role="navigation" aria-label="<?php esc_attr_e( 'Sidebar Menu', 'twentysixteen' ); ?>">
								<?php
									wp_nav_menu( array(
										'theme_location' => 'Sidebar',
										'menu_class'     => 'Sidebar-menu main-menu nav navbar-nav',
									 ) );
								?>
							</nav><!-- .main-navigation -->
							
						<?php endif; ?>
				<?php endif; ?>
				
	<div class="clearfix"></div>
	
	<?php dynamic_sidebar( 'side-menus' ); ?>			
				
</aside>


<!-- ====================Header END here =====================-->	
		

		<div id="content" class="site-content">
