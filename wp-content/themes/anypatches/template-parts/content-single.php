<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

<?php $image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>


<?php if(!is_woocommerce()){ ?>
	<header class="page-title post-title">	
	<div class="container text-center">
        <?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>
	   </div>
	</header><!-- .entry-header -->
<?php } ?>
	
	
	
<div class="post-container <?php if(is_woocommerce()){ echo 'single-product-container'; }?>">
<div class="container">
<div class="row">

<div class="<?php if(!is_woocommerce()){ echo 'col-md-8 col-sm-8'; }else{ echo 'col-md-12 col-sm-12'; }?>">

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	  <div class="breadcrumb"><?php get_breadcrumb(); ?></div>

	<div class="entry-content post-content">
   

  
  	<?php twentysixteen_post_thumbnail ();?>
 
         
        <?php //the_title( '<h2 class="entry-title">', '</h2>' ); ?>
		
		<?php	the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
		
		<div class="entry_meta"><?php twentysixteen_entry_meta(); ?></div>
		
	<?php twentysixteen_excerpt(); ?>
	
	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
	
	

</article><!-- #post-## -->
</div>

<?php if(!is_woocommerce()){ ?>
<div class="col-md-4 col-sm-4 post-sidebar">

	<?php dynamic_sidebar( 'post-sidebar' ); ?>	

</div>
<?php } ?>

</div>
</div>
</div>


