<?php

global $chosen_fields;

$loop = 0;
$current_value = isset( $_POST['addon-' . sanitize_title( $addon['field-name'] ) ] ) ? wc_clean( $_POST[ 'addon-' . sanitize_title( $addon['field-name'] ) ] ) : '';

?>
<p class="form-row form-row-wide addon-wrap-<?php echo sanitize_title( $addon['field-name'] ); ?>">

<!-- Dont add chosen to product type dropdowns -->
<?php  if(in_array($addon['field-name'],['231-product-type-1','228-product-type-1']))  { ?>
	<select class="addon addon-select" name="addon-<?php echo sanitize_title( $addon['field-name'] ); ?>">
<?php } else { ?>
	<select class="addon addon-select <?php echo in_array($addon['field-name'],$chosen_fields)?'chosen':''  ?>" <?php echo in_array($addon['field-name'],$chosen_fields)?'multiple="true"':''  ?> name="addon-<?php echo sanitize_title( $addon['field-name'] ); ?>[]">

<?php } ?>

		<?php if ( empty( $addon['required'] ) ) : ?>
			<option value=""><?php _e('None', 'woocommerce-product-addons'); ?></option>
		<?php else : ?>
			<option value=""><?php _e('Select an option...', 'woocommerce-product-addons'); ?></option>
		<?php endif; ?>

		<?php foreach ( $addon['options'] as $i => $option ) :
			$loop ++;
			$price = apply_filters( 'woocommerce_product_addons_option_price',
				$option['price'] ? '(' . wc_price( get_product_addon_price_for_display( $option['price'] ) ) . ')' : '',
				$option,
				$i,
				'select'
			);
			?>
			<option data-raw-price="<?php echo esc_attr( $option['price'] ); ?>" data-price="<?php echo get_product_addon_price_for_display( $option['price'] ); ?>" value="<?php echo sanitize_title( $option['label'] ) . '-' . $loop; ?>" <?php selected( $current_value, sanitize_title( $option['label'] ) . '-' . $loop ); ?>><?php echo wptexturize( $option['label'] ) ; ?></option>
		<?php endforeach; ?>

	</select>
</p>
